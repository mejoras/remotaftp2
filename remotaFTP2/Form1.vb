﻿Imports System.IO
Imports System.Globalization

Public Class Form1

    'Dim c1 As New Canal
    'Dim c2 As New Canal
    'Dim c3 As New Canal
    'Dim c4 As New Canal
    'Dim c5 As New Canal
    'Dim c6 As New Canal
    'Dim c7 As New Canal
    'Dim c8 As New Canal

    Dim canales As New List(Of Canal)


    Public Sub read2()
        Dim file As String
        Dim ruta As String = Directory.GetCurrentDirectory() + "\29_04_15_15_57_33_BOMBEO_ALA0_FTP_RPT.txt"
        file = My.Computer.FileSystem.ReadAllText(ruta)

        Dim lineas() As String
        lineas = Split(file, vbCrLf)

        'Saco las cabeceras, etc del archivo.
        Dim IniFile As String = "REPORT"
        Dim contIni As Integer = 0
        Dim IniData As String = "DATE"
        Dim contData As Integer = 0

        Dim i As Integer = 0

        Dim found As Boolean = False

        While Not found
            If lineas(i) <> "" And Not IsNothing(lineas(i)) Then
                If lineas(i).Contains(IniFile) Then
                    contIni = i + 1 'indica la posición del teléfono y los canales
                End If
                If lineas(i).Contains(IniData) And i > contIni Then
                    contData = i + 1 'indica la posición de los datos los canales
                    found = True
                End If
            End If
            i += 1
        End While

        'saco los campos de la cabecera datagate del fichero
        Dim campos() As String
        campos = Split(lineas(contIni), vbTab)
        'Me interesa el mapeo de canales y el tlf
        Dim mapeo As String = campos(1)
        Dim tlf As String = "+" + campos(campos.Length - 1)

        'saco los campos de la cabecera de los canales del fichero
        Dim camposCanales() As String
        camposCanales = Split(lineas(contData - 1), vbTab)

        creaCanales(camposCanales, mapeo)

        'Empiezo a leer los datos
        Dim div() As String
        Dim perReg As String = ""
        Dim dateIni, datefin, fecha As DateTime

        For i = contData To lineas.Length - 1
            If lineas(i) <> "" And Not IsNothing(lineas(i)) Then
                div = Split(lineas(i), vbTab)
                fecha = cambiaFechas(div(0))
                If i = contData Then
                    datefin = fecha
                End If
                If perReg = "" And i > contData Then
                    'Lo dejo en HH:mm:ss        
                    perReg = CStr(CInt((DateDiff("s", fecha, datefin) / 3600) Mod 60)) + ":"
                    perReg = perReg + CStr(CInt((DateDiff("s", fecha, datefin) / 60) Mod 60)) + ":"
                    perReg = perReg + CStr(CInt(DateDiff("s", fecha, datefin) Mod 60))
                End If
                'relleno los canales
                LlenaCanales2(div, canales.Count)
            End If
        Next
        dateIni = fecha

        'Creo la cabecera texto que va antes del bloque de datos
        Dim dh As New DatagateHeader
        dh.rellenaMapeoTlf(mapeo, tlf)

        Dim textoIniMsg As String = dh.toString

        'Creo bloque info que va antes de los datos
        Dim textoConfig As String = ""

        Dim listaMsg() As LoggerHeader
        listaMsg = creaConfigCanal(perReg, dateIni, datefin)

        For Each l In listaMsg
            l.ToString()
        Next
        
    End Sub


    'Pone las fechas en formato español
    Private Function cambiaFechas(ByVal str As String) As DateTime
        Dim mydate As DateTime
        Dim fecha() As String
        Dim hora() As String
        Dim tot() As String
        tot = Split(str, " ")
        fecha = Split(tot(0), "/")
        hora = Split(tot(1), ":")
        mydate = New DateTime(fecha(2), fecha(0), fecha(1), hora(0), hora(1), hora(2))
        Return mydate
    End Function

    'Creo los canale sy los añado a la lista de canales
    Public Sub creaCanales(ByVal camposCanales() As String, ByVal mapeo As String)
        Dim Canales As Integer = 0

        For i = 1 To camposCanales.Count - 1
            If Not IsNothing(camposCanales(i)) And camposCanales(i) <> "" Then
                Canales += 1
            End If
        Next

        Dim numCh As Integer
        Dim nomCh As String

        Dim auxChar() As Char
        Dim aux() As String

        Select Case Canales
            Case 1
                aux = Split(camposCanales(1), "_")
                numCh = aux(0)
                nomCh = aux(1)

                Dim c1 As New Canal
                c1.name = nomCh
                c1.number = numCh
                auxChar = mapeo.ToCharArray
                c1.tipo = CInt(auxChar(0).ToString)

                Me.canales.Add(c1)
            Case 2
                auxChar = mapeo.ToCharArray

                aux = Split(camposCanales(1), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c1 As New Canal
                c1.name = nomCh
                c1.number = numCh
                c1.tipo = CInt(auxChar(0).ToString)
                Me.canales.Add(c1)

                aux = Split(camposCanales(2), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c2 As New Canal
                c2.name = nomCh
                c2.number = numCh
                c2.tipo = CInt(auxChar(1).ToString)
                Me.canales.Add(c2)
            Case 3
                auxChar = mapeo.ToCharArray

                aux = Split(camposCanales(1), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c1 As New Canal
                c1.name = nomCh
                c1.number = numCh
                c1.tipo = CInt(auxChar(0).ToString)
                Me.canales.Add(c1)

                aux = Split(camposCanales(2), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c2 As New Canal
                c2.name = nomCh
                c2.number = numCh
                c2.tipo = CInt(auxChar(1).ToString)
                Me.canales.Add(c2)

                aux = Split(camposCanales(3), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c3 As New Canal
                c3.name = nomCh
                c3.number = numCh
                c3.tipo = CInt(auxChar(2).ToString)
                Me.canales.Add(c3)
            Case 4
                auxChar = mapeo.ToCharArray

                aux = Split(camposCanales(1), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c1 As New Canal
                c1.name = nomCh
                c1.number = numCh
                c1.tipo = CInt(auxChar(0).ToString)
                Me.canales.Add(c1)

                aux = Split(camposCanales(2), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c2 As New Canal
                c2.name = nomCh
                c2.number = numCh
                c2.tipo = CInt(auxChar(1).ToString)
                Me.canales.Add(c2)

                aux = Split(camposCanales(3), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c3 As New Canal
                c3.name = nomCh
                c3.number = numCh
                c3.tipo = CInt(auxChar(2).ToString)
                Me.canales.Add(c3)

                aux = Split(camposCanales(4), "_")
                numCh = aux(0)
                nomCh = aux(1)
                Dim c4 As New Canal
                c4.name = nomCh
                c4.number = numCh
                c4.tipo = CInt(auxChar(3).ToString)
                Me.canales.Add(c4)

        End Select


    End Sub

    'Meto los datos leidos en los canales
    Private Sub LlenaCanales2(ByVal div() As String, ByVal numCanales As Integer)

        Dim fecha As DateTime
        fecha = cambiaFechas(div(0))
        Select Case numCanales
            Case 1
                canales.ElementAt(0).Addvalue(fecha, CDbl(div(1).Replace(".", ",")))

            Case 2
                canales.ElementAt(0).Addvalue(fecha, CDbl(div(1).Replace(".", ",")))
                canales.ElementAt(1).Addvalue(fecha, CDbl(div(2).Replace(".", ",")))
                
            Case 3
                canales.ElementAt(0).Addvalue(fecha, CDbl(div(1).Replace(".", ",")))
                canales.ElementAt(1).Addvalue(fecha, CDbl(div(2).Replace(".", ",")))
                canales.ElementAt(2).Addvalue(fecha, CDbl(div(3).Replace(".", ",")))
                
            Case 4
                canales.ElementAt(0).Addvalue(fecha, CDbl(div(1).Replace(".", ",")))
                canales.ElementAt(1).Addvalue(fecha, CDbl(div(2).Replace(".", ",")))
                canales.ElementAt(2).Addvalue(fecha, CDbl(div(3).Replace(".", ",")))
                canales.ElementAt(3).Addvalue(fecha, CDbl(div(4).Replace(".", ",")))
               
            Case 5
                'c1.Addvalue(fecha, div(1))
                'c2.Addvalue(fecha, div(2))
                'c3.Addvalue(fecha, div(3))
                'c4.Addvalue(fecha, div(4))
                'c5.Addvalue(fecha, div(5))
            Case 6
                'c1.Addvalue(fecha, div(1))
                'c2.Addvalue(fecha, div(2))
                'c3.Addvalue(fecha, div(3))
                'c4.Addvalue(fecha, div(4))
                'c5.Addvalue(fecha, div(5))
                'c6.Addvalue(fecha, div(6))
            Case 7
                'c1.Addvalue(fecha, div(1))
                'c2.Addvalue(fecha, div(2))
                'c3.Addvalue(fecha, div(3))
                'c4.Addvalue(fecha, div(4))
                'c5.Addvalue(fecha, div(5))
                'c6.Addvalue(fecha, div(6))
                'c7.Addvalue(fecha, div(7))
            Case 8
                'c1.Addvalue(fecha, div(1))
                'c2.Addvalue(fecha, div(2))
                'c3.Addvalue(fecha, div(3))
                'c4.Addvalue(fecha, div(4))
                'c5.Addvalue(fecha, div(5))
                'c6.Addvalue(fecha, div(6))
                'c7.Addvalue(fecha, div(7))
                'c8.Addvalue(fecha, div(8))
        End Select
    End Sub

    'Crear los canales habilitados en el logger
    Private Function CanalesHabilitadosToHx() As String

        Dim res As String = "0000"

        Dim numC As Integer

        ' Dim bool(7) As Boolean
        Dim bit As New BitArray(7, False)
        Dim bytes() As Byte

        For i = 0 To canales.Count - 1
            numC = canales.ElementAt(i).number
            Select Case numC
                Case 1
                    bit.Set(0, True)
                Case 2
                    bit.Set(1, True)
                Case 3
                    bit.Set(2, True)
                Case 4
                    bit.Set(3, True)
                Case 5
                    bit.Set(4, True)
                Case 6
                    bit.Set(5, True)
                Case 7
                    bit.Set(7, True)
                Case 8
                    bit.Set(8, True)
            End Select
        Next

        ReDim bytes(0)
        bit.CopyTo(bytes, 0)

        Dim byteTest As String = "00"
        'byteTest = CStr(Hex("00")) + CStr(Hex(bytes(0)))
        byteTest = rellenaCeros(Hex(bytes(0)), 2)

        'Invierto los bytes
        byteTest = byteTest.Substring(1, 1) + byteTest.Substring(0, 1)

        byteTest = CStr("00") + byteTest

        Return byteTest
    End Function

    'Crear los canales habilitados en el logger
    Private Function ModoCanalesHabilitadosToHx() As String
        'Si el canal está activo se pone en analogico (bit=1)
        Dim numC As Integer
        Dim tipo As Integer


        Dim bit As New BitArray(7, False)
        Dim bytes() As Byte

        For i = 0 To canales.Count - 1
            numC = canales.ElementAt(i).number
            tipo = canales.ElementAt(i).tipo
            If tipo = 6 Then
                Select Case numC
                    Case 1
                        bit.Set(0, True)
                    Case 2
                        bit.Set(1, True)
                    Case 3
                        bit.Set(2, True)
                    Case 4
                        bit.Set(3, True)
                    Case 5
                        bit.Set(4, True)
                    Case 6
                        bit.Set(5, True)
                    Case 7
                        bit.Set(7, True)
                    Case 8
                        bit.Set(8, True)
                End Select
            End If
        Next

        ReDim bytes(0)
        bit.CopyTo(bytes, 0)


        Dim byteTest As String = "00"
        'byteTest = CStr(Hex("00")) + CStr(Hex(bytes(0)))
        byteTest = rellenaCeros(Hex(bytes(0)), 2)

        'Invierto los bytes
        byteTest = byteTest.Substring(1, 1) + byteTest.Substring(0, 1)

        byteTest = CStr("00") + byteTest

        Return byteTest
    End Function

    Private Function CreaData(ByVal c As Canal) As String
        Dim DataText As String = ""
        Dim dato As String = ""
        Dim I32() As Byte
        'Tengo que recorrer los datos del canal 
        For i = c.valor.Count - 1 To 0 Step -1
            I32 = BitConverter.GetBytes(c.valor(i))

            Dim result As String = String.Join(String.Empty, I32.Select(Function(x) x.ToString("X2")).ToArray())
            'dato = Hex(BitConverter.DoubleToInt64Bits(c.valor(i)))
            DataText = DataText + result
            'Invierto el resultado 
            'DataText = DataText + dato.Substring(2, 2) + dato.Substring(0, 2)
        Next

        Return DataText
    End Function

    Private Function DivideData() As Integer
        Dim DataText As String = ""
        Return DataText
    End Function


    Private Function creaConfigCanal(perReg As String, dateIni As DateTime, datefin As DateTime) As LoggerHeader()
        Dim listaLogger() As LoggerHeader
        Dim c As New Canal

        For i = 0 To canales.Count - 1
            Dim lh As New LoggerHeader
            c = canales.ElementAt(i)
            lh.blocktype = "0C"
            lh.startRecord = FechasToHx(dateIni)
            lh.sampleRate = IntervaloToHx(perReg)
            lh.clockSetting = FechasToHx(dateIni)
            lh.dataStartTime = FechasToHx(dateIni)
            lh.chNum = "00" 'all channels
            lh.enabledCh = CanalesHabilitadosToHx()
            lh.chMode = "0" 'modo conteo
            lh.chType = ModoCanalesHabilitadosToHx()

            Select Case c.tipo
                Case 1 'flow
                    lh.flowCal = IntervaloSegToHx(perReg)
                Case 6 'pressure
                    'Es una constante siempre es 10/1
                    'lh.flowCal = IntervaloAnalogToHx()
                    lh.flowCal = "0A000100"
            End Select

            'Units/Pulse, Offset y meterReading no llevarán nunca valor. los creo a 0.
            lh.cal = "00000000"
            lh.offset = "00000000"
            lh.meterRead = "00000000"


            'DataLenght a 0 para pruebas
            lh.dataLenght = "00"

            'Estos campos son constantes porque son falsos
            lh.alarmStatus = "00000000"
            lh.swMajorPart = Hex(800)
            lh.swMinorPart = Hex(800)
            lh.swMajorVersion = Hex(800)
            lh.swMinorVersion = Hex(800)
            lh.reserved = "00" '¿¿??No sé si deberia meter 2 nulls o que....


            'Aqui debo crear los datos del mensaje y
            lh.data = CreaData(c)


            If IsNothing(listaLogger) Then
                ReDim Preserve listaLogger(0)
                listaLogger(0) = lh
            Else
                ReDim Preserve listaLogger(listaLogger.Length)
                listaLogger(listaLogger.Count - 1) = lh
            End If

        Next

        Return listaLogger
    End Function




    Public Sub read()
        Dim file As String
        Dim ruta As String = Directory.GetCurrentDirectory() + "\29_04_15_15_57_33_BOMBEO_ALA0_FTP_RPT.txt"
        file = My.Computer.FileSystem.ReadAllText(ruta)


        Dim lineas() As String
        lineas = Split(file, vbCrLf)
        Dim campos As String()

        Dim dh As New DatagateHeader

        Dim div() As String

        'saco los campos de la cabecera de la segunda linea del fichero
        campos = Split(lineas(1), vbTab)
        dh.camposCab(campos(1), campos(campos.Length - 1))

        'Leo cabeceras de canales
        Dim cabecerasCanales As String()
        cabecerasCanales = Split(lineas(7), vbTab)


        Dim numCanales As Integer = 0

        Dim dateIni, datefin, fecha As DateTime

        Dim perReg As String = ""

        'Datos
        For i = 8 To lineas.Length - 1
            If lineas(i) <> "" And Not IsNothing(lineas(i)) Then
                div = Split(lineas(i), vbTab)
                If i = 8 Then
                    datefin = cambiaFechas(div(0))
                    numCanales = div.Count - 1
                Else
                    If perReg = "" Then
                        fecha = cambiaFechas(div(0))
                        'Lo dejo en HH:mm:ss        
                        perReg = CStr(CInt((DateDiff("s", fecha, datefin) / 3600) Mod 60)) + ":"
                        perReg = perReg + CStr(CInt((DateDiff("s", fecha, datefin) / 60) Mod 60)) + ":"
                        perReg = perReg + CStr(CInt(DateDiff("s", fecha, datefin) Mod 60))
                    End If
                End If
                'Voy añadiendo los datos a los canales
                LlenaCanales(div, numCanales)
            End If
        Next
        dateIni = cambiaFechas(div(0))


        Dim cabecera As String
        cabecera = dh.formato + dh.telefono + dh.serial + dh.firmware + dh.loggertype + dh.channels + dh.signal + dh.battery + dh.loggerID

        Dim datosComunes As String = "0C"
        ' datosComunes = datosComunes + 
        Dim lh As New LoggerHeader
        lh.blocktype = "0C"
        lh.startRecord = FechasToHx(dateIni)
        lh.sampleRate = IntervaloToHx(perReg)
        lh.clockSetting = FechasToHx(dateIni)
        lh.dataStartTime = FechasToHx(dateIni)
        lh.chNum = "00"

    End Sub

    'Meto los datos leidos en los canales
    Private Sub LlenaCanales(ByVal div() As String, ByVal numCanales As Integer)

        Dim fecha As DateTime
        fecha = cambiaFechas(div(0))
        'Select Case numCanales
        '    Case 1
        '        c1.Addvalue(fecha, div(1))
        '    Case 2
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '    Case 3
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '        c3.Addvalue(fecha, div(3))
        '    Case 4
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '        c3.Addvalue(fecha, div(3))
        '        c4.Addvalue(fecha, div(4))
        '    Case 5
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '        c3.Addvalue(fecha, div(3))
        '        c4.Addvalue(fecha, div(4))
        '        c5.Addvalue(fecha, div(5))
        '    Case 6
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '        c3.Addvalue(fecha, div(3))
        '        c4.Addvalue(fecha, div(4))
        '        c5.Addvalue(fecha, div(5))
        '        c6.Addvalue(fecha, div(6))
        '    Case 7
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '        c3.Addvalue(fecha, div(3))
        '        c4.Addvalue(fecha, div(4))
        '        c5.Addvalue(fecha, div(5))
        '        c6.Addvalue(fecha, div(6))
        '        c7.Addvalue(fecha, div(7))
        '    Case 8
        '        c1.Addvalue(fecha, div(1))
        '        c2.Addvalue(fecha, div(2))
        '        c3.Addvalue(fecha, div(3))
        '        c4.Addvalue(fecha, div(4))
        '        c5.Addvalue(fecha, div(5))
        '        c6.Addvalue(fecha, div(6))
        '        c7.Addvalue(fecha, div(7))
        '        c8.Addvalue(fecha, div(8))
        'End Select
    End Sub



    'Pasar fechas a Hx (vienen en formato MM/dd/yyyy HH:mm:ss)
    Private Function FechasToHx(ByVal str As String) As String
        Dim mydate As String
        Dim ano As String
        Dim fecha() As String
        Dim hora() As String
        Dim tot() As String
        tot = Split(str, " ")
        fecha = Split(tot(0), "/")
        hora = Split(tot(1), ":")
        'Formato ss mm HH dd MM yy 1 byte(2 hx values) por campo
        mydate = rellenaCeros(Hex(hora(2)), 2)
        mydate = mydate + rellenaCeros(Hex(hora(1)), 2)
        mydate = mydate + rellenaCeros(Hex(hora(0)), 2)
        mydate = mydate + rellenaCeros(Hex(fecha(0)), 2)
        mydate = mydate + rellenaCeros(Hex(fecha(1)), 2)

        mydate = mydate + rellenaCeros(Hex(fecha(2).Substring(2, 2)), 2)

        'mydate = Hex(hora(2)) + Hex(hora(1)) + Hex(hora(0)) + Hex(fecha(0)) + Hex(fecha(1)) + Hex(fecha(2).Substring(1, 2))
        Return mydate
    End Function

    'Pasar Periodo de Registro que viene en HH:mm:ss a Hx 
    Private Function IntervaloToHx(ByVal str As String) As String
        Dim myInter As String
        Dim hora() As String
        Dim tot() As String
        tot = Split(str, " ")
        If tot.Count = 1 Then
            hora = Split(str, ":")
        Else
            hora = Split(tot(1), ":")
        End If
        'Formato ss mm HH  1 byte(2 hx values) por campo
        myInter = rellenaCeros(Hex(hora(2)), 2)
        myInter = myInter + rellenaCeros(Hex(hora(1)), 2)
        myInter = myInter + rellenaCeros(Hex(hora(0)), 2)

        'myInter = Hex(hora(2)) + Hex(hora(1)) + Hex(hora(0))
        Return myInter
    End Function

    'Pasar Periodo de Registro que viene en segundos a Hx 
    Private Function IntervaloSegToHx(ByVal str As String) As String
        Dim myInter As String

        Dim hora() As String
        Dim tot() As String

        Dim dateTim As UInteger

        tot = Split(str, " ")
        If tot.Count = 1 Then
            hora = Split(str, ":")
        Else
            hora = Split(tot(1), ":")
        End If

        dateTim = CInt(hora(0)) * 3600 + CInt(hora(1)) * 60 + CInt(hora(0))

        myInter = Hex(dateTim)
        myInter = rellenaCeros(myInter, 4)

        'Invierto el resultado y le añado "/1 = 0100"
        Dim res As String
        res = myInter.Substring(2, 2) + myInter.Substring(0, 2) + CStr("0100")

        Return res
    End Function

    Private Function IntervaloAnalogToHx() As String
        Dim myInter As String

        myInter = Hex(10)

        If myInter.Length < 4 Then
            myInter = rellenaCeros(myInter, 4)
        End If

        'Invierto el resultado y le añado "/1 = 0100"
        Dim res As String
        res = myInter.Substring(2, 2) + myInter.Substring(0, 2) + CStr("0100")

        Return res
    End Function


    Function rellenaCeros(ByVal str As String, ByVal Longitud As Integer) As String
        Dim cont As Integer = Longitud - str.Length
        For i = 0 To cont - 1
            str = CStr("0") + str
        Next
        Return str
    End Function


    'Invertir un string de Hx byte a byte
    Function inversaByteaByte(ByVal valorHX As String) As String
        Dim res As String = ""
        'Se invierten los bytes 
        res = valorHX.Substring(6, 2) + valorHX.Substring(4, 2) + valorHX.Substring(2, 2) + valorHX.Substring(0, 2)
        Return res
    End Function

    'El checksum afecta a todo el mensaje creado
    Function checksum(ByVal valorHX As String) As String
        Dim res As String = ""
        Dim suma As Integer = 0

        Dim notSuma As String = ""
        Dim auxSuma As String = ""

        'sumamos todos los bytes de la cadena
        For i = 0 To valorHX.Length - 1 Step 2
            suma += Val("&H" + valorHX.Substring(i, 2))
        Next
        'Calcula el not de la suma y le suma 1
        notSuma = Hex(Not (Val("&H" + suma.ToString("X"))))
        suma = Val("&H" + notSuma) + 1
        'HAce un AND con FF paraquedarse con las 2 últimas posiciones
        auxSuma = Hex(suma)
        res = Hex(Val("&H" + auxSuma) And Val("&H" + "FF"))
        Return res
    End Function






    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        read2()
    End Sub
End Class
