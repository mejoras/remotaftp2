﻿Public Class LoggerHeader
    Public blocktype As String '1 char(2 Hx)//0C o 0D 
    Public startRecord As String '6 chars(12 Hx) //ssmmhhddmmyy
    Public sampleRate As String '3 chars(6 Hx)
    Public clockSetting As String '6 chars(12Hx) //ssmmhhddmmyy
    Public dataStartTime As String '6 chars(12Hx) //ssmmhhddmmyy
    Public chNum As String '1 chars(2Hx) 
    Public enabledCh As String '2 chars(2 byte integer little endian)
    Public chMode As String '1 chars(2 Hx)
    Public chType As String '2 chars(2 byte integer little endian)
    Public flowCal As String '4 chars(2 byte unsigned little endian))
    Public cal As String '8 chars
    Public offset As String '8 chars
    Public meterRead As String '16 chars
    Public dataLenght As String '2 chars
    Public alarmStatus As String '8 chars
    Public swMajorPart As String '1 chars
    Public swMinorPart As String '1 chars
    Public swMajorVersion As String '1 chars
    Public swMinorVersion As String '1 chars
    Public reserved As String '2 chars
    Public data As String 'max. 412 bytes

    Public Sub New()
        Me.blocktype = "0C"
        Me.startRecord = "**DATA"
        Me.sampleRate = "**DATA"
        Me.clockSetting = "**DATA"
        Me.dataStartTime = "**DATA"
        Me.chNum = "**DATA"
        Me.enabledCh = "**DATA"
        Me.chType = "**DATA"
        Me.flowCal = "**DATA"
        Me.cal = "**DATA"
        Me.offset = "**DATA"
        Me.meterRead = "**DATA"
        Me.dataLenght = "**DATA"
        Me.alarmStatus = "**DATA"
        Me.swMajorPart = "**DATA"
        Me.swMinorPart = "**DATA"
        Me.swMajorVersion = "**DATA"
        Me.swMinorVersion = "**DATA"
        Me.reserved = "**"
        Me.data = "**"
    End Sub

    Public Sub CopyTo(lh As LoggerHeader)
        lh.blocktype = Me.blocktype
        lh.startRecord = Me.startRecord
        lh.sampleRate = Me.sampleRate
        lh.clockSetting = Me.clockSetting
        lh.dataStartTime = Me.dataStartTime
        lh.chNum = Me.chNum
        lh.enabledCh = Me.enabledCh
        lh.chType = Me.chType
        lh.flowCal = Me.flowCal
        lh.cal = Me.cal
        lh.offset = Me.offset
        lh.meterRead = Me.meterRead
        lh.dataLenght = Me.dataLenght
        lh.alarmStatus = Me.alarmStatus
        lh.swMajorPart = Me.swMajorPart
        lh.swMinorPart = Me.swMinorPart
        lh.swMajorVersion = Me.swMajorVersion
        lh.swMinorVersion = Me.swMinorVersion
        lh.reserved = Me.reserved
    End Sub


    Public Overloads Function ToString() As String
        Dim res As String = ""
        res = CStr(Me.blocktype) + CStr(Me.startRecord) + CStr(Me.sampleRate) + CStr(Me.clockSetting) + CStr(Me.dataStartTime) +
            CStr(Me.chNum) + CStr(Me.enabledCh) + CStr(Me.chType) + CStr(Me.flowCal) + CStr(Me.cal) + CStr(Me.offset) + CStr(Me.meterRead) +
            CStr(Me.dataLenght) + CStr(Me.alarmStatus) + CStr(Me.swMajorPart) + CStr(Me.swMinorPart) + CStr(Me.swMajorVersion) +
            CStr(Me.swMinorVersion) + CStr(Me.reserved)
        res = res + CStr(Me.data)
        Return res
    End Function

End Class
