﻿Public Class DatagateHeader

    Public formato As String 'data o alarm
    Public telefono As String
    Public serial As String
    Public firmware As String
    Public loggertype As String
    Public channels As String
    Public signal As String
    Public battery As String
    Public loggerID As String

    Public Sub New()
        Me.formato = "**DATA"
        Me.telefono = "**nnnnnnnnnnnnn"
        Me.serial = "**ssssss"
        Me.firmware = "**vvvvvvv"
        Me.loggertype = "**tttttttttttt"
        Me.channels = "**cc"
        Me.signal = "**gg"
        Me.battery = "**bb"
        Me.loggerID = "**_0000000"
    End Sub


    Public Sub rellenaMapeoTlf(ByVal canales As String, ByVal tlf As String)
        Me.formato = "**" + "DATA"
        Me.telefono = "**" + tlf
        Me.channels = "**" + canales
    End Sub

    Public Overloads Function toString() As String
        Dim result As String = ""

        result = Me.formato + Me.telefono + Me.serial + Me.firmware + Me.loggertype + Me.channels + Me.signal + Me.battery + Me.loggerID
    
        Return result
    End Function


    'Public Sub New(ByVal form As String, ByVal tlf As String, ByVal firm As String,
    '               ByVal type As String, ByVal cha As String, ByVal sig As String, ByVal bat As String, ByVal ID As String)
    '    Me.formato = "**" + form
    '    Me.telefono = "**" + tlf
    '    Me.firmware = "**" + firm
    '    Me.loggertype = "**" + type
    '    Me.channels = "**" + cha
    '    Me.signal = "**" + sig
    '    Me.battery = "**" + bat
    '    Me.loggerID = "**" + ID
    'End Sub

    Public Sub camposCab(ByVal canales As String, ByVal tlf As String)
        Me.formato = "**" + "DATA"
        Me.telefono = "**" + tlf
        Me.channels = "**" + canales
    End Sub


End Class
