﻿

Public Class Canal
    Public name As String
    Public number As Integer '1-8
    Public tipo As Integer '6 o 1 (presion o caudal)
    Public fechahora() As DateTime
    Public valor() As Double

    Public Sub Addvalue(fh As DateTime, val As Double)
        If IsNothing(Me.fechahora) Then
            ReDim fechahora(0)
            fechahora(0) = fh

            ReDim valor(0)
            valor(0) = val
        Else
            ReDim Preserve fechahora(fechahora.Length)
            fechahora(fechahora.Count - 1) = fh

            ReDim Preserve valor(valor.Length)
            valor(valor.Count - 1) = val
        End If
    End Sub



End Class
